﻿using Individual1.Task4.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task4.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix matrix = ExampleCreate();
            ExampleMinor(matrix);
            ExampleOperations(matrix, new Matrix(new double[,] { { 1, 1, 11 }, { 4, 2, 6 } }));

            Console.ReadKey();
        }

        private static Matrix ExampleCreate()
        {
            Matrix matrix = new Matrix(new double[,] { { 1, 2, 3 }, { 4, 5, 6 } });

            Console.WriteLine("Matrix: ");
            Console.Write(matrix);
            Console.WriteLine("Rows: {0}, Columns: {1}", matrix.Rows, matrix.Columns);
            Console.WriteLine("Element with coordinates [1; 2]: " + matrix[1, 2]);

            return matrix;
        }

        private static void ExampleMinor(Matrix matrix)
        {
            Console.WriteLine("Minor of the [1;1]: ");
            Console.Write(matrix.GetMinor(1, 1));

            Console.WriteLine("Minor of the 2: ");
            Console.Write(matrix.GetMinor(2));
        }

        private static void ExampleOperations(Matrix m1, Matrix m2)
        {
            Console.WriteLine();

            Console.WriteLine("Example of adding: ");
            Console.Write(m1 + m2);

            Console.WriteLine();
            Console.WriteLine("Example of subtraction: ");
            Console.Write(m1 - m2);
        }
    }
}
