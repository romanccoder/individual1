﻿using Individual1.Task1.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task1.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberVector vector1 = VectorBaseExample();
            NumberVector vector2 = VectorAddExample(vector1);
            VectorSubstractExample(vector1, vector2);
            VectorEqualsExample(vector1, vector2);
            VectorOutOfRangeExample(vector1);
            VectorDifferentSizeExample();

            Console.ReadKey();
        }

        private static NumberVector VectorBaseExample()
        {
            NumberVector vector1 = new NumberVector(5, 3);
            vector1[5] = 1;
            vector1[6] = 2;
            vector1[7] = 3;
            Console.WriteLine("Assume we have a first vector of numbers: ");
            Console.WriteLine(vector1);
            Console.WriteLine("The total numbers of elements is " + vector1.Size);
            Console.WriteLine("The start index of first element is " + vector1.StartIndex);
            Console.WriteLine("The 6th element is " + vector1[6]);
            vector1.Multiply(2);
            Console.WriteLine("We can multiply all elements with constant (2) :");
            Console.WriteLine(vector1);

            return vector1;
        }

        private static NumberVector VectorAddExample(NumberVector vector1)
        {
            NumberVector vector2 = new NumberVector(5, 3);
            vector2[5] = 4;
            vector2[6] = 5;
            vector2[7] = 6;
            Console.WriteLine();
            Console.WriteLine("Assume we have a second vector of numbers: ");
            Console.WriteLine(vector2);

            Console.WriteLine("Then we can add two vectors: ");
            NumberVector vector3 = vector1 + vector2;
            Console.WriteLine(vector3);

            return vector2;
        }

        private static void VectorSubstractExample(NumberVector vector1, NumberVector vector2)
        {
            Console.WriteLine();
            Console.WriteLine("Also we can subtract two vectors: ");
            NumberVector vector4 = vector1 - vector2;
            Console.WriteLine(vector4);
        }

        private static void VectorEqualsExample(NumberVector vector1, NumberVector vector2)
        {
            Console.WriteLine();
            Console.WriteLine("Also we can check vectors equality: ");
            Console.WriteLine(vector1 == vector2);
        }

        private static void VectorOutOfRangeExample(NumberVector vector1)
        {
            Console.WriteLine();
            Console.WriteLine("We can't access non-existent index: ");
            try
            {
                Console.WriteLine(vector1[5000]);
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void VectorDifferentSizeExample()
        {
            Console.WriteLine();
            Console.WriteLine("We can't add or subtract vectors with different sizes: ");
            NumberVector vector1 = new NumberVector(2);
            NumberVector vector2 = new NumberVector(10);

            try
            {
                Console.WriteLine("Addition: ");
                Console.WriteLine(vector1 + vector2);
            }
            catch (DifferentSizeException e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Console.WriteLine("Subtraction: ");
                Console.WriteLine(vector1 - vector2);
            }
            catch (DifferentSizeException e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
