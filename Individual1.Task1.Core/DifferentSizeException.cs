﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task1.Core
{
    public class DifferentSizeException : ArgumentException
    {
        public DifferentSizeException(string message) : base(message)
        { }

        public DifferentSizeException(string message, Exception innerException) : base(message, innerException)
        { }

    }
}
